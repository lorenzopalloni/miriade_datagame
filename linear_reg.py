from my_functions import *
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import OneHotEncoder

RANDOM_STATE = 42

train = pd.read_csv("/home/null/PycharmProjects/Miriade/data/dataset_processed.csv")
test = pd.read_csv("/home/null/PycharmProjects/Miriade/data/testset_processed.csv")

X = train.iloc[:, [0, 3, 4, 5, 8, 9, 10, 12, 13, 14, 15, 16, 18]]
y = train.iloc[:, 11]

X_test = test.iloc[:, [0, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 17]]

for x in train.columns:
    pass

enc = OneHotEncoder()
enc.fit(X)